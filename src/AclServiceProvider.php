<?php
namespace Fororgs\Acl;

use Blade;
use Illuminate\Support\ServiceProvider;

class AclServiceProvider extends ServiceProvider
{
	/**
	 * Indicates of loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the service provider
	 *
	 * @return null
	 */
	public function boot()
	{
		$this->publishes([
			__DIR__.'/../migrations' => $this->app->databasePath().'/migrations'
		], 'migrations');

		$this->registerBladeDirectives();
	}

	/**
	 * Register the service provider
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('acl', function ($app) {
			$auth = $app->make('Illuminate\Contracts\Auth\Guard');

			return new \Fororgs\Acl\Acl($auth);
		});

		$this->configureAncillarySetup();
	}

	/**
	 * Register the blade directives.
	 *
	 * @return void
	 */
	protected function registerBladeDirectives()
	{
		Blade::directive('can', function($expression) {
			return "<?php if (\\Acl::can({$expression})): ?>";
		});

		Blade::directive('endcan', function($expression) {
			return "<?php endif; ?>";
		});

		Blade::directive('canatleast', function($expression) {
			return "<?php if (\\Acl::canAtLeast({$expression})): ?>";
		});

		Blade::directive('endcanatleast', function($expression) {
			return "<?php endif; ?>";
		});

		Blade::directive('role', function($expression) {
			return "<?php if (\\Acl::is({$expression})): ?>";
		});

		Blade::directive('endrole', function($expression) {
			return "<?php endif; ?>";
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return ['acl'];
	}

	/**
	 * Configure Ancillary Setup
	 *
	 * Registers the necessary Fororgs Acl tags with external plugin;
	 * only if it is setup.
	 *
	 * @return null
	 */
	protected function configureAncillarySetup()
	{
		// $engine = $this->app['config']->get('themes.engine');

		// if ($engine == 'twig') {
		// 	$this->app['config']->push('sapling.tags', 'Fororgs\Acl\Twig\TokenParser\Twig_TokenParser_Can');
		// 	$this->app['config']->push('sapling.tags', 'Fororgs\Acl\Twig\TokenParser\Twig_TokenParser_CanAtLeast');
		// 	$this->app['config']->push('sapling.tags', 'Fororgs\Acl\Twig\TokenParser\Twig_TokenParser_Role');
		// }
	}
}
